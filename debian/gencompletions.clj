(ns gencompletions
  (:require clojure.set)
  (:require clojure.xml)
  (:require clojure.zip))

(def completions
    (reduce concat (map (fn [p] (keys (ns-publics (find-ns p))))
                        '(clojure.core clojure.set clojure.xml clojure.zip))))
 
(print (apply str (interleave completions (repeat "\n"))))
